<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="user_email_unique",columns={"email"})})
 */
class User implements UserInterface {
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"user", "auth-token", "group", "notif", "trading"})
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Groups({"user", "auth-token", "group", "notif", "trading", "offer"})
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"user", "auth-token", "group", "notif", "trading", "offer"})
     */
    private $lastname;

    /**
     * @ORM\Column(type="string")
     * @Groups({"user", "auth-token", "trading", "offer"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $password;

    private $plainPassword;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"user", "auth-token"})
     */
    private $dateOfBirth;


    /**
     * @ORM\Column(type="string")
     * @Groups({"user", "auth-token", "group", "notif", "trading", "offer"})
     * @var string
     */
    private $picture;

    /**
     * @ORM\Column(type="boolean",nullable=true)
     * @Groups({"user", "auth-token"})
     * @var bool
     */
    private $authWithGoogle = false;

    /**
     * @ORM\Column(type="array")
     * @Groups({"user", "auth-token"})
     */
    private $roles = ['ROLE_USER'];

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Interest", inversedBy="users",cascade={"persist"})
     * @Groups({"user", "auth-token"})
     */
    private $interests;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Trading", inversedBy="users", cascade={"persist"})
     */
    private $tradings;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Location", cascade={"persist"})
     * @Groups({"user", "auth-token"})
     */
    private $location;

    /**
     * Les groupes dont l'utilisateur est l'admin
     * @ORM\OneToMany(targetEntity="SocialGroup", mappedBy="admin")
     */
    private $createdGroups;

    /**
     * Les groupes dont fait partie l'utilisateur
     * @ORM\ManyToMany(targetEntity="SocialGroup", inversedBy="members")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $groups;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Notification", mappedBy="recipient", cascade={"persist"})
     */
    private $notifications;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string Firebase Cloud Messaging token
     * @Groups({"user", "auth-token"})
     */
    private $fcmToken;

    public function __construct() {
        $this->interests = new ArrayCollection();
        $this->tradings = new ArrayCollection();
        $this->tradings = new ArrayCollection();
        $this->createdGroups = new ArrayCollection();
        $this->groups = new ArrayCollection();
        $this->notifications = new ArrayCollection();

        $this->picture = "default-profile.png";
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFirstname() {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname): void {
        $this->firstname = $firstname;
    }

    /**
     * @return mixed
     */
    public function getLastname() {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname): void {
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getDateOfBirth() {
        return $this->dateOfBirth;
    }

    /**
     * @param mixed $dateOfBirth
     */
    public function setDateOfBirth($dateOfBirth): void {
        $this->dateOfBirth = $dateOfBirth;
    }

    /**
     * @return mixed
     */
    public function getPlainPassword() {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     */
    public function setPlainPassword($plainPassword): void {
        $this->plainPassword = $plainPassword;
    }


    /**
     * @return string
     */
    public function getPicture(): string {
        return $this->picture;
    }

    /**
     * @param string $picture
     */
    public function setPicture(string $picture): void {
        $this->picture = $picture;
    }

    /**
     * @return bool
     */
    public function isAuthWithGoogle(): bool {
        return $this->authWithGoogle;
    }

    /**
     * @param bool $authWithGoogle
     */
    public function setAuthWithGoogle(bool $authWithGoogle): void {
        $this->authWithGoogle = $authWithGoogle;
    }


    public function getRoles() {
        return $this->roles;
    }

    /**
     * @param mixed $roles
     */
    public function setRoles($roles): void {
        $this->roles = $roles;
    }

    /**
     * @return ArrayCollection
     */
    public function getInterests() {
        return $this->interests;
    }

    /**
     * @return ArrayCollection
     */
    public function getTradings() {
        return $this->tradings;
    }

    /**
     * @param mixed $interests
     */
    public function setInterests($interests): void {
        $this->interests = $interests;
    }


    public function addInterest(Interest $interest) {
        $this->interests[] = $interest;
        $interest->addUser($this);
    }

    public function removeInterest(Interest $interest) {
        $this->interests->removeElement($interest);
        $interest->removeUser($this);
    }


    /**
     * @return \App\Entity\Location
     */
    public function getLocation() {
        return $this->location;
    }

    /**
     * @param mixed
     */
    public function setLocation($location) {
        $this->location = $location;
    }


    public function addTrading(Trading $trading) {
        $this->tradings[] = $trading;
    }

    public function removeTrading(Trading $trading) {
        $this->tradings->removeElement($trading);
    }

    /**
     * @return ArrayCollection
     */
    public function getCreatedGroups() {
        return $this->createdGroups;
    }

    /**
     * @return mixed
     */
    public function getGroups() {
        return $this->groups;
    }


    public function addCreatedGroup(SocialGroup $group) {
        $this->createdGroups[] = $group;
    }

    public function removeCreatedGroup(SocialGroup $group) {
        $this->createdGroups->removeElement($group);
    }

    public function addGroup(SocialGroup $group) {
        $this->groups[] = $group;
    }

    public function removeGroup(SocialGroup $group) {
        $this->groups->removeElement($group);
    }


    public function addNotification(Notification $notification) {
        $this->notifications[] = $notification;
    }

    public function removeNotification(Notification $notification) {
        $this->notifications->removeElement($notification);
    }

    /**
     * @return ArrayCollection
     */
    public function getNotifications() {
        return $this->notifications;
    }


    public function getSalt() {
        return null;
    }

    public function getUsername() {
        return $this->email;
    }

    public function eraseCredentials() {
        // Suppression des données sensibles
        $this->plainPassword = null;
    }

    protected function getUploadRootDir() {
        // On retourne le chemin relatif vers l'image pour notre code PHP
        return dirname(__FILE__, 3) . '/public/img';
    }

    public function getWebPath() {
        return $this->getUploadRootDir() . '/' . $this->getPicture();
    }

    /**
     * @return string
     */
    public function getFcmToken(): ?string {
        return $this->fcmToken;
    }

    /**
     * @param string $fcmToken
     */
    public function setFcmToken(string $fcmToken): void {
        $this->fcmToken = $fcmToken;
    }

    public function containsTrading($id): bool {
        foreach ($this->getTradings() as $trading) {
            if ($trading->getId() == $id) {
                return true;
            }
        }
        return false;
    }
}
