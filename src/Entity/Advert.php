<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AdvertRepository")
 */
class Advert {
    public const MEDIA_IMAGE = 'IMAGE';
    public const MEDIA_VIDEO = 'VIDEO';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"advert"})
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Groups({"advert"})
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"advert"})
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Interest")
     * @Groups({"advert"})
     */
    private $category;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"advert"})
     */
    private $content;

    /**
     * @ORM\Column(type="string")
     * @Groups({"advert"})
     */
    private $type;

    /**
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    private $file;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $user;

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getCategory() {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category): void {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content): void {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void {
        $this->type = $type;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    public function getFile() {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file): void {
        $this->file = $file;
    }

    public function getUploadRootDir() {
        // On retourne le chemin relatif vers l'image pour notre code PHP
        return dirname(__FILE__, 3) . '/public/img';
    }

    public function getWebPath() {
        return 'img/' . $this->getContent();
    }



    public function upload() {
        $directory = $this->getUploadRootDir() . '/' . $this->getUser()
                ->getId();
        if ($this->file == null) {
            return 1;
        }
        $name = $this->file->getFilename();
        $extension = $this->file->getClientOriginalExtension();
        $name .= '.' . $extension;
        if (in_array($extension, ['jpg', 'jpeg', 'png', 'gif'])) {
            $this->type = self::MEDIA_IMAGE;
        }
        elseif (in_array($extension, ['mp4', '3gp', 'mkv', 'flv'])) {
            $this->type = self::MEDIA_VIDEO;
        }
        else {
            return 2;
        }


        if ($this->file->move($directory, $name) == null) {
            return 2;
        }
        $this->setContent($this->getUser()->getId() . '/' . $name);
        return 0;
    }

    /**
     * @return mixed
     */
    public function getUser(): User {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void {
        $this->user = $user;
    }
}
