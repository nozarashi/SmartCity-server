<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SocialGroupRepository")
 */
class SocialGroup {
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"group", "notif"})
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Groups({"group", "notif"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"group"})
     */
    private $description;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"group"})
     */
    private $image;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="createdGroups")
     * @Groups({"group"})
     */
    private $admin;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="groups")
     * @Groups({"group"})
     */
    private $members;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="group", cascade={"persist"})
     * @ORM\OrderBy({"createdAt" = "DESC"})
     * @Groups({"group"})
     */
    private $posts;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     * @Groups({"group"})
     */
    private $isPublic;

    public function __construct() {
        $this->members = new ArrayCollection();
        $this->posts = new ArrayCollection();
        $this->isPublic = true;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void {
        $this->description = $description;
    }



    /**
     * @return \App\Entity\User
     */
    public function getAdmin() {
        return $this->admin;
    }

    /**
     * @param mixed $admin
     */
    public function setAdmin($admin): void {
        $this->admin = $admin;
    }

    /**
     * @return mixed
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void {
        $this->image = $image;
    }



    public function addMember(User $user) {
        $this->members[] = $user;
    }

    public function removeMember(User $user) {
        $this->members->removeElement($user);
    }

    public function addPost(Post $post) {
        $this->posts[] = $post;
    }

    public function removePost(Post $post) {
        $this->posts->removeElement($post);
    }

    /**
     * @return mixed
     */
    public function getMembers() {
        return $this->members;
    }

    /**
     * @return mixed
     */
    public function getPosts() {
        return $this->posts;
    }

    /**
     * @return bool
     */
    public function isPublic(): bool {
        return $this->isPublic;
    }

    /**
     * @param bool $isPublic
     */
    public function setIsPublic(bool $isPublic): void {
        $this->isPublic = $isPublic;
    }



}
