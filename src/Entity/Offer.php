<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OfferRepository")
 */
class Offer {
    public const MEDIA_IMAGE = 'IMAGE';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"trading", "offer"})
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Groups({"trading", "offer"})
     */
    private $product;

    /**
     * @ORM\Column(type="string")
     * @Groups({"trading", "offer"})
     */
    private $brand;

    /**
     * @ORM\Column(type="decimal", scale=2)
     * @Groups({"trading", "offer"})
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Trading", inversedBy="offers")
     * @Groups({"offer"})
     */
    private $trading;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Interest")
     * @Groups({"trading", "offer"})
     */
    private $category;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"trading", "offer"})
     */
    private $image;

    /**
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    private $file;

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getProduct() {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product): void {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getBrand() {
        return $this->brand;
    }

    /**
     * @param mixed $brand
     */
    public function setBrand($brand): void {
        $this->brand = $brand;
    }

    /**
     * @return mixed
     */
    public function getPrice() {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void {
        $this->price = $price;
    }

    /**
     * @return \App\Entity\Trading
     */
    public function getTrading(): Trading {
        return $this->trading;
    }

    /**
     * @param mixed $trading
     */
    public function setTrading($trading): void {
        $this->trading = $trading;
    }

    /**
     * @return mixed
     */
    public function getCategory() {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category): void {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void {
        $this->image = $image;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\File\UploadedFile | null
     */
    public function getFile(): ?\Symfony\Component\HttpFoundation\File\UploadedFile {
        return $this->file;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     */
    public function setFile(\Symfony\Component\HttpFoundation\File\UploadedFile $file): void {
        $this->file = $file;
    }

    public function getUploadRootDir() {
        // On retourne le chemin relatif vers l'image pour notre code PHP
        return dirname(__FILE__, 3) . '/public/img';
    }

    public function getWebPath() {
        return 'img/' . $this->getImage();
    }

    public function upload() {
        $directory = $this->getUploadRootDir() . '/' . $this->getTrading()
                ->getUser()
                ->getId();
        if ($this->file == null) {
            return 1;
        }
        $name = $this->file->getFilename();
        $extension = $this->file->getClientOriginalExtension();
        $name .= '.' . $extension;
        if (!in_array($extension, ['jpg', 'jpeg', 'png', 'gif'])) {
            return 2;
        }


        if ($this->file->move($directory, $name) == null) {
            return 2;
        }
        $this->setImage($this->getTrading()->getUser()->getId() . '/' . $name);
        return 0;
    }
}
