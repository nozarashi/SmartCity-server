<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ORM\Entity(repositoryClass="App\Repository\TradingRepository")
 */
class Trading {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"trading", "offer"})
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Groups({"trading", "offer"})
     */
    private $name;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Location", cascade={"persist"})
     * @Groups({"trading", "offer"})
     */
    private $location;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"trading", "offer"})
     */
    private $completeAddress;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"trading", "offer"})
     */
    private $streetNumber;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"trading", "offer"})
     */
    private $route;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"trading", "offer"})
     */
    private $postalCode;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"trading", "offer"})
     */
    private $latitude;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"trading", "offer"})
     */
    private $longitude;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @Groups({"trading", "offer"})
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offer", mappedBy="trading", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @Groups({"trading"})
     */
    private $offers;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="tradings")
     * @Groups({"trading"})
     */
    private $users;

    public function __construct() {
        $this->offers = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void {
        $this->id = $id;
    }


    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void {
        $this->name = $name;
    }

    /**
     * @return \App\Entity\Location
     */
    public function getLocation() {
        return $this->location;
    }

    /**
     * @param \App\Entity\Location $location
     */
    public function setLocation($location): void {
        $this->location = $location;
    }


    /**
     * @return ArrayCollection
     */
    public function getOffers() {
        return $this->offers;
    }

    public function addOffer(Offer $offer) {
        $this->offers[] = $offer;
    }

    public function removeOffer(Trading $offer) {
        $this->offers->removeElement($offer);
    }

    /**
     * @return mixed
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void {
        $this->user = $user;
    }

    /**
     * @return ArrayCollection
     */
    public function getUsers() {
        return $this->users;
    }

    /**
     * @param mixed $users
     */
    public function setUsers($users): void {
        $this->users = $users;
    }

    public function addUser(User $user) {
        $this->users[] = $user;
    }

    public function removeUser(Trading $user) {
        $this->users->removeElement($user);
    }

    /**
     * @return mixed
     */
    public function getCompleteAddress() {
        return $this->completeAddress;
    }

    /**
     * @param mixed $completeAddress
     */
    public function setCompleteAddress($completeAddress): void {
        $this->completeAddress = $completeAddress;
    }

    /**
     * @return mixed
     */
    public function getLongitude() {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude): void {
        $this->longitude = $longitude;
    }

    /**
     * @return mixed
     */
    public function getStreetNumber() {
        return $this->streetNumber;
    }

    /**
     * @param mixed $streetNumber
     */
    public function setStreetNumber($streetNumber): void {
        $this->streetNumber = $streetNumber;
    }

    /**
     * @return mixed
     */
    public function getRoute() {
        return $this->route;
    }

    /**
     * @param mixed $route
     */
    public function setRoute($route): void {
        $this->route = $route;
    }

    /**
     * @return mixed
     */
    public function getLatitude() {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude): void {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function getPostalCode() {
        return $this->postalCode;
    }

    /**
     * @param mixed $postalCode
     */
    public function setPostalCode($postalCode): void {
        $this->postalCode = $postalCode;
    }
}
