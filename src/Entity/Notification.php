<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NotificationRepository")
 */
class Notification {

    const NOTIFICATION_TYPES = ['JOIN GROUP'];
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"notif"})
     */
    private $id;

    /**
     * L'utilisateur déclenchant l'envoi de notification
     * @var \App\Entity\User
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @Groups({"notif"})
     */
    private $sender;

    /**
     * L'utilisateur déclenchant l'envoi de notification
     * @var \App\Entity\User
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="notifications")
     */
    private $recipient;

    /**
     * @ORM\Column(type="text")
     * @Groups({"notif"})
     */
    private $message;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SocialGroup")
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     * @Groups({"notif"})
     */
    private $group;

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSender() {
        return $this->sender;
    }

    /**
     * @param mixed $sender
     */
    public function setSender($sender): void {
        $this->sender = $sender;
    }

    /**
     * @return mixed
     */
    public function getRecipient() {
        return $this->recipient;
    }

    /**
     * @param mixed $recipient
     */
    public function setRecipient($recipient): void {
        $this->recipient = $recipient;
    }

    /**
     * @return mixed
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message): void {
        $this->message = $message;
    }



    /**
     * @return mixed
     */
    public function getGroup() {
        return $this->group;
    }

    /**
     * @param mixed $group
     */
    public function setGroup($group): void {
        $this->group = $group;
    }

}
