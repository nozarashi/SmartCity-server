<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InterestRepository")
 */
class Interest
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"user", "auth-token", "trading", "offer"})
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     *  @Groups({"user", "auth-token", "trading", "offer"})
     */
    private $category;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="interests")
     */
    private $users;

    public function __construct() {
        $this->users = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCategory() {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category): void {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getUsers() {
        return $this->users;
    }

    /**
     * @param mixed $users
     */
    public function setUsers($users): void {
        $this->users = $users;
    }


    public function addUser(User $user) {
        $this->users[] = $user;
    }

    public function removeUser(User $user) {
        $this->users->removeElement($user);
    }

}
