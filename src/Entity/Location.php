<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LocationRepository")
 */
class Location {
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"user", "auth-token", "trading", "offer"})
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Groups({"user", "auth-token", "trading", "offer"})
     */
    private $city;

    /**
     * @ORM\Column(type="string")
     * @Groups({"user", "auth-token", "trading", "offer"})
     */
    private $country;

    /**
     * @ORM\Column(type="string")
     * @Groups({"user", "auth-token", "trading", "offer"})
     */
    private $countryLongName;

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCity() {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city): void {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getCountry() {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country): void {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getCountryLongName() {
        return $this->countryLongName;
    }

    /**
     * @param mixed $countryLongName
     */
    public function setCountryLongName($countryLongName): void {
        $this->countryLongName = $countryLongName;
    }


}
