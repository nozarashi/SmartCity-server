<?php

namespace App\Controller;

use App\Entity\Advert;
use App\Entity\Interest;
use App\Form\AdvertType;
use App\Utils\NotificationManager;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Pagerfanta\Adapter\DoctrineORMAdapter;


class AdvertController extends Controller {
    /**
     * @Route("/adverts", defaults={"page": "1"}, name="adverts")
     * @Route("/adverts/page/{page}", requirements={"page": "[1-9]\d*"}, name="adverts_index_paginated")
     * @Method({"GET"})
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index(int $page) {
        $user = $this->getUser();
        if (empty($user)) {
            return $this->redirectToRoute('login');
        }
        $em = $this->get('doctrine.orm.entity_manager');

        $adverts = $em->getRepository('App:Advert')
            ->findUserAdverts($user->getId(), $page);

        return $this->render('advert/index.html.twig', [
            'adverts' => $adverts
        ]);
    }

    /**
     * @Route("/add-adverts", name="add_advert")
     * @Method({"GET", "POST"})
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     */
    public function addAdvert(Request $request) {
        $user = $this->getUser();
        if (empty($user)) {
            return $this->redirectToRoute('login');
        }
        $advert = new Advert();
        $em = $this->get('doctrine.orm.entity_manager');
        $form = $this->createForm(AdvertType::class, $advert)
            ->add('Valider', SubmitType::class, ['attr' => ['class' => 'btn btn-primary']]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $advert->setUser($user);
            $res = $advert->upload();
            switch ($res) {
                case 1 || 2:
                    $request->getSession()
                        ->getFlashBag()
                        ->add('error', 'Veuillez fournir une image ou une video');
                    break;
                default:
                    $em->persist($advert);
                    $em->flush();
                    $tokenList = [];
                    foreach ($advert->getCategory()->getUsers() as $u) {
                        $tokenList[] = $u->getFcmToken();
                    }
                    $notifManager = NotificationManager::getInstance();
                    $msg = $advert->getTitle();
                    $notifManager->sendNotification('Nouvelle Pub !', $msg, $tokenList);
                    return $this->redirectToRoute('adverts');
            }
        }
        return $this->render('advert/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/adverts/{id}/edit", name="edit_advert")
     * @Method({"GET", "POST"})
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     */
    public function editAdvert(Request $request) {
        $id = $request->get('id');
        $user = $this->getUser();
        if (empty($user)) {
            return $this->redirectToRoute('login');
        }
        $em = $this->get('doctrine.orm.entity_manager');
        $advert = $em->getRepository('App:Advert')
            ->find($id);

        if (empty($advert)) {
            return $this->redirectToRoute('adverts');
        }

        $oldFile = !empty($advert->getContent()) ? $advert->getUploadRootDir() . '/' . $advert->getContent(): '';

        $form = $this->createForm(AdvertType::class, $advert)
            ->add('Valider', SubmitType::class, ['attr' => ['class' => 'btn btn-primary']]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $res = $advert->upload();
            switch ($res) {
                case 1:
                    break;
                case 2:
                    $request->getSession()
                        ->getFlashBag()
                        ->add('error', 'Veuillez fournir une image ou une video');
                    break;
                default:
                    if (file_exists($oldFile)) {
                        unlink($oldFile);
                    }
                    break;
            }
            $em->flush();
            return $this->redirectToRoute('adverts');
        }
        return $this->render('advert/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/adverts/{id}/delete", name="delete_advert")
     * @Method({"GET", "POST"})
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     */
    public function deleteAdvert(Request $request) {
        $id = $request->get('id');
        $user = $this->getUser();
        if (empty($user)) {
            return $this->redirectToRoute('login');
        }
        $em = $this->get('doctrine.orm.entity_manager');
        $advert = $em->getRepository('App:Advert')
            ->find($id);

        if (empty($advert)) {
            return $this->redirectToRoute('adverts');
        }

        // On supprime le fichier
        $oldFile = $advert->getUploadRootDir() . '/' . $advert->getContent();

        if (file_exists($oldFile)) {
            unlink($oldFile);
        }
        $em->remove($advert);
        $em->flush();
        return $this->redirectToRoute('adverts');
    }
}
