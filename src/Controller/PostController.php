<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Notification;
use App\Entity\Post;
use App\Form\PostType;
use App\Utils\FormErrors;
use App\Utils\NotificationManager;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PostController extends Controller {
    use FormErrors;

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"group"})
     * @Rest\Post("/api/groups/{id}/posts")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \App\Entity\SocialGroup|\FOS\RestBundle\View\View|null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postPostAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $group = $em->getRepository('App:SocialGroup')
            ->find($request->get('id'));
        if ($group) {
            $post = new Post();
            $form = $this->createForm(PostType::class, $post, [
                'validation_groups' => [
                    'Default',
                    'New'
                ]
            ]);

            $token = $em->getRepository('App:AuthToken')
                ->findOneBy([
                    'value' => $request->headers->get('X-Auth-Token')
                ]);

            /** @var \App\Entity\User $user */
            $user = $token->getUser();
            $form->submit($request->request->all());
            if ($form->isValid()) {
                if (($group
                        ->getMembers()
                        ->contains($user)) || ($group
                            ->getAdmin()
                            ->getId() == $user->getId())) {
                    $post->setGroup($group);
                    $post->setUser($user);
                    $group->addPost($post);

                    // Envoi des notifications
                    $notification = new Notification();
                    $notification->setMessage('NEW POST');
                    $notification->setGroup($group);
                    $notification->setSender($user);
                    $notification->setRecipient($group->getAdmin());
                    $group->getAdmin()->addNotification($notification);
                    $tokenList = [$group->getAdmin()->getFcmToken()];
                    foreach ($group->getMembers() as $member) {
                        if ($member->getId() !== $user->getId()) {
                            $notif = new Notification();
                            $notif->setMessage('NEW POST');
                            $notif->setGroup($group);
                            $notif->setSender($user);
                            $notif->setRecipient($member);
                            $member->addNotification($notif);
                            $tokenList[] = $member->getFcmToken();
                        }
                    }
                    $em->flush();
                    $notifManager = NotificationManager::getInstance();
                    $title = 'Nouveau Post de ' . $user->getFirstname() . ' ' . $user->getLastname() . ' dans le groupe ' . $group->getName() . '.';
                    $msg = $post->getTitle();
                    $notifManager->sendNotification($title, $msg, $tokenList);
                    return $group;
                }
                else {
                    return View::create(['message' => 'You are not a member of this group'], Response::HTTP_UNAUTHORIZED);
                }

            }
            else {
                return View::create($this->getErrors($form), Response::HTTP_BAD_REQUEST);
            }
        }
        return $this->postNotFound();
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"group"})
     * @Rest\Post("/api/posts/{id}/comments")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \App\Entity\Post|\FOS\RestBundle\View\View|null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postCommentAction(Request $request) {
        $content = $request->get('content');
        $comment = new Comment();
        $em = $this->get('doctrine.orm.entity_manager');
        $token = $em->getRepository('App:AuthToken')
            ->findOneBy([
                'value' => $request->headers->get('X-Auth-Token')
            ]);

        $user = $token->getUser();

        $post = $em->getRepository('App:Post')
            ->find($request->get('id'));
        if ($post) {
            if (($post->getGroup()
                    ->getMembers()
                    ->contains($user)) || ($post->getGroup()
                        ->getAdmin()
                        ->getId() == $user->getId())) {

                $comment->setUser($user);
                $comment->setPost($post);
                $post->addComment($comment);
                $comment->setContent($content);
                $em->flush();

                return $post;
            }
            else {
                return View::create(['message' => 'You are not a member of this group'], Response::HTTP_UNAUTHORIZED);
            }

        }
        return $this->postNotFound();
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT, serializerGroups={"group"})
     * @Rest\Delete("/api/posts/{id}")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \FOS\RestBundle\View\View
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public
    function removePostAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $token = $em->getRepository('App:AuthToken')
            ->findOneBy([
                'value' => $request->headers->get('X-Auth-Token')
            ]);

        $post = $em->getRepository('App:Post')
            ->find($request->get('id'));
        if ($post) {
            if (($post->getUser()->getId() == $token->getUser()
                        ->getId()) || $post->getGroup()
                    ->getAdmin()
                    ->getId()) {
                $em->remove($post);
                $em->flush();

            }
            else {
                return View::create(['message' => 'You are not a member of this group'], Response::HTTP_UNAUTHORIZED);
            }
        }
        else {
            return $this->postNotFound();
        }
    }

    private function postNotFound() {
        return View::create(['message' => 'Post not found'], Response::HTTP_NOT_FOUND);
    }
}
