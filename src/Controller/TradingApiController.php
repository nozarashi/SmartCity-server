<?php

namespace App\Controller;

use App\Entity\Offer;
use App\Entity\Trading;
use App\Entity\User;
use App\Form\OfferType;
use App\Form\TradingType;
use App\Utils\FormErrors;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TradingApiController extends Controller {

    use FormErrors;

    /**
     * Retourne la liste des commerces appartenant à un utilisateur
     * @Rest\View(serializerGroups={"trading"})
     * @Rest\Get("/api/tradings")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \App\Entity\Notification[]|\App\Entity\Trading[]|array
     */
    public function getTradingsAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');

        $token = $em->getRepository('App:AuthToken')
            ->findOneBy([
                'value' => $request->headers->get('X-Auth-Token')
            ]);
        $user = $token->getUser();
        if (empty($request->get('interest')) && ($user->getLocation() == null)) {
            $tradings = $em->getRepository('App:Trading')
                ->findAll();
            return $tradings;
        }
        if(empty($request->get('interest')) && ($user->getLocation() != null)) {
            $location = $user->getLocation()->getId();
            $tradings = $em->getRepository('App:Trading')
                ->findAllTradings($location);
            return $tradings;
        }
        if ($user->getLocation() == null) {
            $location = null;
        }
        else {
            $location = $user->getLocation()->getId();
        }

        $tradings = $em->getRepository('App:Trading')
            ->findTradingsByInterest($request->get('interest'), $location);
        return $tradings;
    }


    /**
     * Retourne la liste des commerces appartenant à un utilisateur
     * @Rest\View(serializerGroups={"trading"})
     * @Rest\Get("/api/tradings/search/{name}")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \App\Entity\Notification[]|\App\Entity\Trading[]|array
     */
    public function getTradingsByNameAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');

        $token = $em->getRepository('App:AuthToken')
            ->findOneBy([
                'value' => $request->headers->get('X-Auth-Token')
            ]);
        $user = $token->getUser();
        $tradings = $em->getRepository('App:Trading')
            ->findTradingsByName($request->get('name'));
        return $tradings;

    }

    /**
     * Retourne un seul commerce
     * @Rest\View(serializerGroups={"trading"})
     * @Rest\Get("/api/tradings/{id}")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \App\Entity\Trading|null|object|static
     */
    public function getTradingAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $trading = $em->getRepository('App:Trading')->find($request->get('id'));
        if (empty($trading)) {
            return $this->tradingNotFound();
        }
        return $trading;
    }

    /**
     * @Rest\View(serializerGroups={"trading"})
     * @Rest\Post("/api/tradings")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \App\Entity\Trading|\FOS\RestBundle\View\View
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postTradingsAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $token = $em->getRepository('App:AuthToken')
            ->findOneBy([
                'value' => $request->headers->get('X-Auth-Token')
            ]);
        $user = $token->getUser();

        $trading = new Trading();
        $form = $this->createForm(TradingType::class, $trading, [
            'validation_groups' => [
                'Default',
                'New'
            ]
        ]);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $trading->setUser($user);
            $interest = $em->getRepository('App:Interest')
                ->findOneBy([
                    'category' => $request->get('category')
                ]);
            if ($interest) {
                $trading->setCategory($interest);
            }

            //Location
            $city = ($request->get('location'))['city'];
            $country = ($request->get('location'))['country'];
            $countryLongName = ($request->get('location'))['countryLongName'];
            $location = $em->getRepository('App:Location')->findOneBy([
                'city' => $city,
                'country' => $country,
                'countryLongName' => $countryLongName
            ]);
            if ($location) {
                $trading->setLocation($location);
            }
            $em->persist($trading);
            $em->flush();
            return $trading;
        }
        else {
            return View::create($this->getErrors($form), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT, serializerGroups={"trading"})
     * @Rest\Delete("/api/tradings/{id}")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeTradingsAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $trading = $em->getRepository('App:Trading')->find($request->get('id'));
        if ($trading) {
            $em->remove($trading);
            $em->flush();
        }
    }

    /**
     * @Rest\View(serializerGroups={"trading"})
     * @Rest\Patch("/api/tradings/{id}")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \App\Entity\Trading|null|object|static
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function patchTradingAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $trading = $em->getRepository('App:Trading')->find($request->get('id'));
        if (empty($trading)) {
            return $this->tradingNotFound();
        }
        $form = $this->createForm(TradingType::class, $trading);
        $form->submit($request->request->all(), false);
        if ($form->isValid()) {


            //Location
            $city = ($request->get('location'))['city'];
            $country = ($request->get('location'))['country'];
            $countryLongName = ($request->get('location'))['countryLongName'];
            $location = $em->getRepository('App:Location')->findOneBy([
                'city' => $city,
                'country' => $country,
                'countryLongName' => $countryLongName
            ]);
            if ($location) {
                $trading->setLocation($location);
            }

            $em->flush();
            return $trading;
        }
        else {
            return View::create($this->getErrors($form), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Retourne les offres d'un commerce
     * @Rest\View(serializerGroups={"trading"})
     * @Rest\Get("/api/tradings/{id}/offers")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return null|object
     */
    public function getOffersAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $trading = $em->getRepository('App:Trading')->find($request->get('id'));
        return $trading->getOffers();
    }

    /**
     * Retourne une offre d'un commerce
     * @Rest\View(serializerGroups={"trading"})
     * @Rest\Get("/api/tradings/{id}/offers/{offer_id}")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \App\Entity\Offer|\FOS\RestBundle\View\View|null|object
     */
    public function getOfferAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $trading = $em->getRepository('App:Trading')->find($request->get('id'));
        if (empty($trading)) {
            return $this->tradingNotFound();
        }
        $offer = $em->getRepository('App:Offer')->findOneBy([
            'id' => $request->get('id'),
            'trading' => $trading
        ]);
        if (empty($offer)) {
            return $this->offerNotFound();
        }
        return $offer;
    }

    /**
     * Créer une offre
     * @Rest\View(serializerGroups={"trading"})
     * @Rest\Post("/api/tradings/{id}/offers")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \App\Entity\Offer|\FOS\RestBundle\View\View
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postOfferAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $trading = $em->getRepository('App:Trading')->find($request->get('id'));
        if (empty($trading)) {
            return $this->tradingNotFound();
        }
        $offer = new Offer();
        $form = $this->createForm(OfferType::class, $offer, [
            'validation_groups' => [
                'Default',
                'New'
            ]
        ]);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $interest = $em->getRepository('App:Interest')
                ->findOneBy([
                    'category' => $request->get('category')
                ]);
            if ($interest) {
                $offer->setCategory($interest);
            }
            $offer->setTrading($trading);
            $trading->addOffer($offer);
            $em->flush();
            return $offer;
        }
        else {
            return View::create($this->getErrors($form), Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * @Rest\View(serializerGroups={"trading"})
     * @Rest\Patch("/api/tradings/{id}/offers/{offer_id}")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \App\Entity\Offer|\FOS\RestBundle\View\View|null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function patchOfferAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $trading = $em->getRepository('App:Trading')->find($request->get('id'));
        if (empty($trading)) {
            return $this->tradingNotFound();
        }
        $offer = $em->getRepository('App:Offer')->findOneBy([
            'id' => $request->get('offer_id'),
            'trading' => $trading
        ]);
        if (empty($offer)) {
            return $this->offerNotFound();
        }
        $form = $this->createForm(OfferType::class, $offer);
        $form->submit($request->request->all(), false);

        if ($form->isValid()) {
            $interest = $em->getRepository('App:Interest')
                ->findOneBy([
                    'category' => $request->get('category')
                ]);
            if ($interest) {
                $offer->setCategory($interest);
            }
            $em->flush();
            return $offer;
        }
        else {
            return View::create($this->getErrors($form), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT,serializerGroups={"trading"})
     * @Rest\Delete("/api/tradings/{id}/offers/{offer_id}")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \FOS\RestBundle\View\View
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeOfferAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $trading = $em->getRepository('App:Trading')->find($request->get('id'));
        if (empty($trading)) {
            return $this->tradingNotFound();
        }
        $offer = $em->getRepository('App:Offer')->findOneBy([
            'id' => $request->get('offer_id'),
            'trading' => $trading
        ]);
        if (empty($offer)) {
            return $this->offerNotFound();
        }
        $em->remove($offer);
        $em->flush();
    }

    private function userNotFound() {
        return View::create(['message' => 'User not found'], Response::HTTP_NOT_FOUND);
    }

    private function tradingNotFound() {
        return View::create(['message' => 'Trading not found'], Response::HTTP_NOT_FOUND);
    }

    private function offerNotFound() {
        return View::create(['message' => 'Offer not found'], Response::HTTP_NOT_FOUND);
    }
}
