<?php

namespace App\Controller;

use App\Entity\Notification;
use App\Entity\SocialGroup;
use App\Form\GroupType;
use App\Utils\FormErrors;
use App\Utils\NotificationManager;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class GroupController extends Controller {

    use FormErrors;

    /**
     * @Rest\View(serializerGroups={"group"})
     * @Rest\Get("/api/users/{id}/groups")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Doctrine\Common\Collections\ArrayCollection|\FOS\RestBundle\View\View
     */
    public function getGroupsAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');

        $user = $em->getRepository('App:User')
            ->find($request->get('id'));
        if ($user) {
            $groups = new ArrayCollection(array_merge($user->getCreatedGroups()
                ->toArray(), $user->getGroups()->toArray()));
            return $groups;
        }
        return $this->userNotFound();
    }

    /**
     * @Rest\View(serializerGroups={"group"})
     * @Rest\Get("/api/groups/{name}")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \App\Entity\SocialGroup[] | array
     */
    public function getGroupsByNameAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $groups = $em->getRepository('App:SocialGroup')
            ->findGroupsByName($request->get('name'));
        return $groups;
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"group"})
     * @Rest\Post("/api/users/{id}/groups")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \App\Entity\SocialGroup|\FOS\RestBundle\View\View
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postGroupAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em
            ->getRepository('App:User')
            ->find($request->get('id'));

        if ($user) {
            $group = new SocialGroup();
            $form = $this->createForm(GroupType::class, $group, [
                'validation_groups' => [
                    'Default',
                    'New'
                ]
            ]);
            $form->submit($request->request->all());
            if ($form->isValid()) {
                $group->setAdmin($user);
                $em->persist($group);
                $em->persist($user);
                $em->flush();

                return $group;
            }
            else {
                return View::create($this->getErrors($form), Response::HTTP_BAD_REQUEST);
            }
        }
        return $this->userNotFound();
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"group"})
     * @Rest\Post("/api/users/{id}/groups/{group_id}/image")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \FOS\RestBundle\View\View
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postGroupImage(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('App:User')
            ->find($request->get('id'));
        if ($user) {
            $group = $em->getRepository('App:SocialGroup')
                ->findOneBy([
                    'id' => $request->get('group_id'),
                    'admin' => $user
                ]);
            if ($group) {

                $path = $_SERVER['DOCUMENT_ROOT'] . '/SmartCity-server/public/img/' . $group->getImage();
                if (!empty($group->getImage()) && file_exists($path)) {
                    unlink($path);
                }
                $path = $_SERVER['DOCUMENT_ROOT'] . '/SmartCity-server/public/img/' . $user->getId();

                if (!is_dir($path)) {
                    mkdir($path);
                }

                $tmp = explode(".", $_FILES['upload']['name']);
                $extension = end($tmp);
                $filename = 'group-' . str_replace(" ", "-", $group->getName()) . '-' . $group->getId() . '.' . $extension;

                $file_path = $path . '/' . $filename;
                $error = false;

                if (move_uploaded_file($_FILES['upload']['tmp_name'], $file_path)) {
                    $group->setImage($user->getId() . '/' . $filename);
                    $em->flush();
                    return $group;
                }
                else {
                    $error = true;
                }
                if ($error) {
                    return View::create(['message' => 'Unable to save file'], Response::HTTP_UNPROCESSABLE_ENTITY);
                }
            }
        }
        return $this->userNotFound();
    }

    /**
     * @Rest\View(serializerGroups={"group"})
     * @Rest\Patch("/api/groups/{id}")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \App\Entity\SocialGroup|\FOS\RestBundle\View\View|null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function patchGroupAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $group = $em->getRepository('App:SocialGroup')
            ->find($request->get('id'));
        if (empty($group)) {
            return $this->groupNotFound();
        }
        $form = $this->createForm(GroupType::class, $group);
        $form->submit($request->request->all(), false);
        if ($form->isValid()) {
            $em->flush();
            return $group;
        }
        else {
            return View::create($this->getErrors($form), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_OK, serializerGroups={"group"})
     * @Rest\Post("/api/groups/{id}/members")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \App\Entity\SocialGroup|\FOS\RestBundle\View\View|null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addMemberAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $group = $em->getRepository('App:SocialGroup')
            ->find($request->get('id'));
        if ($group) {
            $token = $em->getRepository('App:AuthToken')
                ->findOneBy([
                    'value' => $request->headers->get('X-Auth-Token')
                ]);
            $user = $token->getUser();
            if ($group->getAdmin()->getId() == $user->getId()) {
                $member = $em->getRepository('App:User')
                    ->find($request->get('member'));
                if ($member) {
                    if (!$group->getMembers()
                            ->contains($member)) {
                        $group->addMember($member);
                        $member->addGroup($group);
                        $notif = $em->getRepository('App:Notification')
                            ->find($request->get('notification'));
                        if (!empty($notif)) {
                            $em->remove($notif);
                        }
                        $em->flush();
                        $notifManager = NotificationManager::getInstance();
                        $msg = ' Votre demande pour rejoindre le groupe ' . $group->getName() . ' a été accepté.';
                        $notifManager->sendNotification('Demande acceptée !', $msg, [$member->getFcmToken()]);
                        return $group;
                    }
                    else {
                        return View::create(['message' => 'Already member'], Response::HTTP_UNPROCESSABLE_ENTITY);
                    }

                }
                return $this->userNotFound();
            }

            return View::create(['message' => 'You are not a member of this group'], Response::HTTP_UNAUTHORIZED);
        }
        else {
            return $this->groupNotFound();
        }
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_OK, serializerGroups={"group"})
     * @Rest\Post("/api/groups/{id}/ask-to-join")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function askToJoinGroupAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $group = $em->getRepository('App:SocialGroup')
            ->find($request->get('id'));
        if ($group) {
            $token = $em->getRepository('App:AuthToken')
                ->findOneBy([
                    'value' => $request->headers->get('X-Auth-Token')
                ]);
            /** @var \App\Entity\User $user */
            $user = $token->getUser();
            $notif = $em->getRepository('App:Notification')
                ->findOneBy([
                    'sender' => $user,
                    'recipient' => $group->getAdmin(),
                    'group' => $group,
                    'message' => 'JOIN GROUP'
                ]);
            if (!$notif) {
                $notification = new Notification();
                $notification->setMessage('JOIN GROUP');
                $notification->setGroup($group);
                $notification->setSender($user);
                $notification->setRecipient($group->getAdmin());
                $group->getAdmin()->addNotification($notification);
                $em->persist($notification);
                $em->flush();
                $notifManager = NotificationManager::getInstance();
                $msg = $user->getFirstname() . ' ' . $user->getLastname() . ' a demandé à rejoindre le groupe ' . $group->getName() . '.';
                $notifManager->sendNotification('Demande !', $msg, [$group->getAdmin()->getFcmToken()]);
            }
        }
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_OK, serializerGroups={"group"})
     * @Rest\Delete("/api/groups/{id}/members/{member}")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \App\Entity\SocialGroup|\FOS\RestBundle\View\View|null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeMemberAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $group = $em->getRepository('App:SocialGroup')
            ->find($request->get('id'));
        if ($group) {
            $token = $em->getRepository('App:AuthToken')
                ->findOneBy([
                    'value' => $request->headers->get('X-Auth-Token')
                ]);
            $user = $token->getUser();
            if ($group->getAdmin()->getId() == $user->getId()) {
                $member = $em->getRepository('App:User')
                    ->find($request->get('member'));
                if ($member) {
                    if ($group->getMembers()->contains($member)) {
                        $group->removeMember($member);
                        $member->removeGroup($group);
                        $em->flush();
                        return $group;
                    }
                    else {
                        return View::create(['message' => 'This user was not a member'], Response::HTTP_UNPROCESSABLE_ENTITY);
                    }

                }
                return $this->userNotFound();
            }

            return View::create(['message' => 'You are not a member of this group'], Response::HTTP_UNAUTHORIZED);
        }
        else {
            return $this->groupNotFound();
        }
    }

    /**
     * @Rest\View(serializerGroups={"notif"})
     * @Rest\Get("/api/notifications")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \App\Entity\Notification[]|array
     */
    public function getUserNotificationsAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $token = $em->getRepository('App:AuthToken')
            ->findOneBy([
                'value' => $request->headers->get('X-Auth-Token')
            ]);

        $user = $token->getUser();

        $notifs = $em->getRepository('App:Notification')->findBy(
            [
                'recipient' => $user
            ]
        );

        return $notifs;
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/api/notifications/{id}")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @throws \Doctrine\ORM\ORMException
     */
    public function removeNotificationAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $notif = $em->getRepository('App:Notification')
            ->find($request->get('id'));
        if (!empty($notif)) {
            $em->remove($notif);
        }
        $em->flush();

    }

    private function userNotFound() {
        return View::create(['message' => 'User not found'], Response::HTTP_NOT_FOUND);
    }

    private function groupNotFound() {
        return View::create(['message' => 'Group not found'], Response::HTTP_NOT_FOUND);
    }
}
