<?php

namespace App\Controller;

use App\Entity\Interest;
use App\Entity\Location;
use App\Entity\Offer;
use App\Entity\Trading;
use App\Form\InterestType;
use App\Form\OfferType;
use App\Form\TradingType;
use App\Utils\NotificationManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TradingController extends Controller {
    // dirname(__FILE__,3);
    /**
     * @Route("/", defaults={"page": "1"}, name="tradings")
     * @Route("/page/{page}", requirements={"page": "[1-9]\d*"}, name="tradings_index_paginated")
     * @Method({"GET"})
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index(int $page) {


        $user = $this->getUser();
        if (empty($user)) {
            return $this->redirectToRoute('login');
        }
        $em = $this->get('doctrine.orm.entity_manager');
        $tradings = $em->getRepository('App:Trading')
            ->findUserTradings($user->getId(), $page);

        // $this->sendNotification();
        return $this->render('trading/index.html.twig', [
            'tradings' => $tradings,
        ]);
    }

    /**
     * @Route("/tradings", name="add_trading")
     * @Method({"GET", "POST"})
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     */
    public function addAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $trading = new Trading();
        $form = $this->createForm(TradingType::class, $trading)
            ->add('Valider', SubmitType::class, ['attr' => ['class' => 'btn btn-primary']]);
        $form->remove('id');

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $trading->setUser($this->getUser());
            $city = $trading->getLocation()->getCity();
            $country = $trading->getLocation()->getCountry();
            $countryLongName = $trading->getLocation()->getCountryLongName();
            $location = $em->getRepository('App:Location')->findOneBy([
                'city' => $city,
                'country' => $country,
                'countryLongName' => $countryLongName
            ]);
            if ($location) {
                $trading->setLocation($location);
            }
            else {
                $newLocation = new Location();
                $newLocation->setCity($city);
                $newLocation->setCountry($country);
                $newLocation->setCountryLongName($countryLongName);
                $trading->setLocation($newLocation);
            }
            $em->persist($trading);
            $em->flush();
            return $this->redirectToRoute('tradings');
        }
        return $this->render('trading/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/tradings/{id}", defaults={"page": "1"},name="show_trading")
     * @Route("/tradings/{id}/page/{page}", requirements={"page": "[1-9]\d*"}, name="offers_index_paginated")
     * @Method({"GET"})
     * @param int $id
     * @param int $page
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function showAction(int $id, int $page, Request $request) {
        $user = $this->getUser();
        if (empty($user)) {
            return $this->redirectToRoute('login');
        }
        $em = $this->get('doctrine.orm.entity_manager');
        $trading = $em->getRepository('App:Trading')
            ->findOneBy([
                'user' => $user,
                'id' => $id
            ]);
        $offers = $em->getRepository('App:Offer')
            ->findTradingOffers($id, $page);

        return $this->render('trading/show.html.twig', [
            'trading' => $trading,
            'offers' => $offers
        ]);
    }

    /**
     * @Route("/tradings/{id}/edit", name="edit_trading")
     * @Method({"GET", "POST"})
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function editAction(Request $request) {
        $user = $this->getUser();
        if (empty($user)) {
            return $this->redirectToRoute('login');
        }
        $em = $this->get('doctrine.orm.entity_manager');
        $trading = $em->getRepository('App:Trading')
            ->findOneBy([
                'user' => $user,
                'id' => $request->get('id')
            ]);
        $form = $this->createForm(TradingType::class, $trading)
            ->add('Valider', SubmitType::class, ['attr' => ['class' => 'btn btn-primary']]);
        $form->remove('id');

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $city = $trading->getLocation()->getCity();
            $country = $trading->getLocation()->getCountry();
            $countryLongName = $trading->getLocation()->getCountryLongName();
            $location = $em->getRepository('App:Location')->findOneBy([
                'city' => $city,
                'country' => $country,
                'countryLongName' => $countryLongName
            ]);
            if ($location) {
                $trading->setLocation($location);
            }
            else {
                $newLocation = new Location();
                $newLocation->setCity($city);
                $newLocation->setCountry($country);
                $newLocation->setCountryLongName($countryLongName);
                $em->persist($newLocation);
                $trading->setLocation($newLocation);
            }
            $em->flush();
            return $this->redirectToRoute('tradings');
        }

        return $this->render('trading/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/tradings/{id}/delete", name="delete_trading")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function deleteAction(Request $request) {
        $user = $this->getUser();
        if (empty($user)) {
            return $this->redirectToRoute('login');
        }
        $em = $this->get('doctrine.orm.entity_manager');
        $trading = $em->getRepository('App:Trading')
            ->findOneBy([
                'user' => $user,
                'id' => $request->get('id')
            ]);
        $em->remove($trading);
        $em->flush();
        return $this->redirectToRoute('tradings');
    }


    /**
     * @Route("/tradings/{id}/offer", name="add_offer")
     * @Method({"GET", "POST"})
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addOfferAction(Request $request) {
        $user = $this->getUser();
        if (empty($user)) {
            return $this->redirectToRoute('login');
        }
        $em = $this->get('doctrine.orm.entity_manager');
        $trading = $em->getRepository('App:Trading')->find($request->get('id'));
        if (empty($trading)) {
            return $this->redirectToRoute('tradings');
        }
        $offer = new Offer();
        $offer->setTrading($trading);

        $form = $this->createForm(OfferType::class, $offer);
        $form->remove('id');
        $form->remove('category');
        $form->add('category', EntityType::class, [
            'class' => Interest::class,
            'choice_label' => 'category',
            'attr' => ['class' => 'form-control'],
            'label' => 'Catégorie'
        ])
            ->add('Valider', SubmitType::class, [
                'attr' => ['class' => 'btn btn-primary']
            ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $res = $offer->upload();
            switch ($res) {
                case 2:
                    $request->getSession()
                        ->getFlashBag()
                        ->add('error', 'Veuillez fournir une image');
                    break;
                default:
                    $em->persist($offer);
                    $em->flush();
                    $tokenList = [];
                    foreach ($trading->getUsers() as $u) {
                        $tokenList[] = $u->getFcmToken();
                    }
                    $notifManager = NotificationManager::getInstance();
                    $msg = $offer->getProduct().' à '.$offer->getPrice(). ' chez '.$trading->getName();
                    $notifManager->sendNotification('Nouvelle offre !', $msg, $tokenList);
                    return $this->redirectToRoute('tradings');
            }
        }
        return $this->render('offer/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/tradings/{id}/offer/{offer_id}/edit", name="edit_offer")
     * @Method({"GET", "POST"})
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function editOfferAction(Request $request) {
        $user = $this->getUser();
        if (empty($user)) {
            return $this->redirectToRoute('login');
        }
        $em = $this->get('doctrine.orm.entity_manager');
        $trading = $em->getRepository('App:Trading')->find($request->get('id'));
        if (empty($trading)) {
            return $this->redirectToRoute('tradings');
        }
        $offer = $em->getRepository('App:Offer')->findOneBy([
            'id' => $request->get('offer_id'),
            'trading' => $trading
        ]);
        if (empty($offer)) {
            return $this->redirectToRoute('show_trading', ['id' => $request->get('id')]);
        }

        $oldFile = !empty($offer->getImage()) ? $offer->getUploadRootDir() . '/' . $offer->getImage() : '';
        $oldPrice = $offer->getPrice();

        $form = $this->createForm(OfferType::class, $offer);
        $form->remove('id');
        $form->remove('category');
        $form->add('category', EntityType::class, [
            'class' => Interest::class,
            'choice_label' => 'category',
            'attr' => ['class' => 'form-control'],
            'label' => 'Catégorie'
        ])
            ->add('Valider', SubmitType::class, [
                'attr' => ['class' => 'btn btn-primary']
            ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $res = $offer->upload();
            switch ($res) {
                case 1:
                    break;
                case 2:
                    $request->getSession()
                        ->getFlashBag()
                        ->add('error', 'Veuillez fournir une image ou une video');
                    break;
                default:
                    if (file_exists($oldFile)) {
                        unlink($oldFile);
                    }
                    break;
            }
            if ($offer->getPrice() < $oldPrice) {
                $tokenList = [];
                foreach ($offer->getTrading()->getUsers() as $u) {
                    $tokenList[] = $u->getFcmToken();
                }
                $notifManager = NotificationManager::getInstance();
                $msg = $offer->getProduct() . ' passe à ' . $offer->getPrice() . ' chez ' . $trading->getName() . '.';
                $notifManager->sendNotification('Promotions !', $msg, $tokenList);
            }
            $em->flush();
            return $this->redirectToRoute('show_trading', ['id' => $request->get('id')]);
        }
        return $this->render('offer/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/tradings/{id}/offer/{offer_id}/delete", name="delete_offer")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function deleteOfferAction(Request $request) {
        $user = $this->getUser();
        if (empty($user)) {
            return $this->redirectToRoute('login');
        }
        $em = $this->get('doctrine.orm.entity_manager');
        $trading = $em->getRepository('App:Trading')->find($request->get('id'));
        if (empty($trading)) {
            return $this->redirectToRoute('tradings');
        }
        $offer = $em->getRepository('App:Offer')->findOneBy([
            'id' => $request->get('offer_id'),
            'trading' => $trading
        ]);
        if (empty($offer)) {
            return $this->redirectToRoute('show_trading', ['id' => $request->get('id')]);
        }
        $em->remove($offer);
        $em->flush();
        return $this->redirectToRoute('tradings');
    }
}
