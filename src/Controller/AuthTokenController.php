<?php

namespace App\Controller;


use App\Entity\AuthToken;
use App\Entity\Credentials;
use App\Entity\User;
use App\Form\CredentialsType;
use App\Utils\FormErrors;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Google_Client;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class AuthTokenController extends Controller {

    use FormErrors;

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"auth-token"})
     * @Rest\Post("/api/auth-tokens")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \App\Entity\AuthToken|\FOS\RestBundle\View\View
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function postAuthTokensAction(Request $request) {
        $credentials = new Credentials();
        $form = $this->createForm(CredentialsType::class, $credentials);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return View::create($this->getErrors($form), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $em = $this->get('doctrine.orm.entity_manager');

        $user = $em->getRepository('App:User')
            ->findOneByEmail($credentials->getLogin());

        if (!$user) { // L'utilisateur n'existe pas
            return $this->invalidCredentials();
        }

        $encoder = $this->get('security.password_encoder');
        $isPasswordValid = $encoder->isPasswordValid($user, $credentials->getPassword());

        if (!$isPasswordValid) { // Le mot de passe n'est pas correct
            return $this->invalidCredentials();
        }

        $authToken = new AuthToken();
        $authToken->setValue(base64_encode(random_bytes(50)));
        $authToken->setCreatedAt(new \DateTime('now'));
        $authToken->setUser($user);
        $user->setRoles(['ROLE_USER']);

        $em->persist($authToken);
        $em->merge($user);
        $em->flush();

        return $authToken;
    }

    private function invalidCredentials() {
        return View::create([
            'message' => 'Invalid credentials',
            'errors' => []
        ], Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"auth-token"})
     * @Rest\Post("/api/google-sign-in")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \App\Entity\AuthToken|\FOS\RestBundle\View\View
     * @throws \Doctrine\ORM\ORMException
     * @throws \Exception
     */
    public function googleSignInAction(Request $request) {
        // Get $id_token via HTTPS POST.
        $id_token = $request->get('idToken');
        $CLIENT_ID = getenv('GOOGLE_CLIENT_ID');
        $client = new Google_Client(['client_id' => $CLIENT_ID]);  // Specify the CLIENT_ID of the app that accesses the backend
        $payload = $client->verifyIdToken($id_token);
        if ($payload) {
            $userid = $payload['sub'];
            $email = $payload['email'];
            $firstname = $payload['given_name'];
            $lastname = $payload['family_name'];
            $pictureUrl = $payload['picture'];

            $em = $this->get('doctrine.orm.entity_manager');

            $user = $em->getRepository('App:User')->findOneByEmail($email);

            if ($user) {
                $authToken = new AuthToken();
                $authToken->setValue(base64_encode(random_bytes(50)));
                $authToken->setCreatedAt(new \DateTime('now'));
                $authToken->setUser($user);
                $em->persist($authToken);
                $em->flush();
                return $authToken;
            }

            $user = new User();
            $user->setFirstname($firstname);
            $user->setLastname($lastname);
            $user->setEmail($email);
            $user->setAuthWithGoogle(true);
            $picture = file_get_contents($pictureUrl);

            $authToken = new AuthToken();
            $authToken->setValue(base64_encode(random_bytes(50)));
            $authToken->setCreatedAt(new \DateTime('now'));
            $authToken->setUser($user);
            $user->setRoles(['ROLE_USER']);

            $em->persist($user);
            $em->persist($authToken);
            $em->flush();

            // on récupère l'image du profil
            if ($picture !== false) {
                $dir = __DIR__ . '/../../public/img/' . $user->getId();
                if (!is_dir($dir)) {
                    mkdir($dir);
                }
                $size = getimagesize($pictureUrl);
                $extension = image_type_to_extension($size[2]);
                file_put_contents($dir . '/profile'.$extension, $picture);
                $user->setPicture($user->getId().'/profile'.$extension);
                $em->merge($user);
                $em->flush();
            }

            return $authToken;
        }
        else {
            return View::create(['message' => 'Invalid token'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/api/auth-tokens/{id}")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeAuthTokenAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $authToken = $em->getRepository('App:AuthToken')
            ->find($request->get('id'));
        /* @var $authToken AuthToken */

        $connectedUser = $this->get('security.token_storage')
            ->getToken()
            ->getUser();

        if ($authToken && $authToken->getUser()
                ->getId() === $connectedUser->getId()) {
            $em->remove($authToken);
            $em->flush();
        }
        else {
            throw new BadRequestHttpException();
        }
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_OK, serializerGroups={"auth-token-only"})
     * @Rest\Post("/api/refresh")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \App\Entity\AuthToken|\FOS\RestBundle\View\View
     * @throws \Exception
     */
    public function refreshAction(Request $request) {
        $token = $request->get('token');
        $userId = $request->get('userId');
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('App:User')->findOneById($userId);
        $expiredToken = $em->getRepository('App:AuthToken')->findBy([
            'value' => $token,
            'user' => $user
        ]);

        if($expiredToken) {
            $authToken = new AuthToken();
            $authToken->setValue(base64_encode(random_bytes(50)));
            $authToken->setCreatedAt(new \DateTime('now'));
            $authToken->setUser($user);
            foreach ($expiredToken as $t) {
                $em->remove($t);
            }

            $em->persist($authToken);
            $em->flush();
            return $authToken;
        }
        return View::create(['message' => 'Auth Token is not valid'], Response::HTTP_UNAUTHORIZED);
    }
}