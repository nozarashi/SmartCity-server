<?php

namespace App\Form;

use App\Entity\Location;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LocationType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('city', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'autocomplete' => 'user-password'
                ]
            ])
            ->add('countryLongName', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ],
                'label' => 'Country'
            ])
            ->add('country', HiddenType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
                'label' => 'Country code'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            // uncomment if you want to bind to a class
            'data_class' => Location::class,
            'csrf_protection' => false
        ]);
    }
}
