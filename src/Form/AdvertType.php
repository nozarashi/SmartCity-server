<?php

namespace App\Form;

use App\Entity\Advert;
use App\Entity\Interest;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdvertType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('title', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('description', TextareaType::class, [
                'attr' => ['class' => 'form-control'],
                'required' => false
            ])
            ->add('category', EntityType::class, [
                'class' => Interest::class,
                'choice_label' => 'category',
                'attr' => ['class' => 'form-control'],
                'label' => 'Catégorie'
            ])
            ->add('file', FileType::class, [
                'label' => 'Contenu Pub',
                'required' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            // uncomment if you want to bind to a class
            'data_class' => Advert::class,
        ]);
    }
}
