<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180409095339 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE trading_offer');
        $this->addSql('ALTER TABLE offer ADD trading_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE offer ADD CONSTRAINT FK_29D6873E97CD2A32 FOREIGN KEY (trading_id) REFERENCES trading (id)');
        $this->addSql('CREATE INDEX IDX_29D6873E97CD2A32 ON offer (trading_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE trading_offer (trading_id INT NOT NULL, offer_id INT NOT NULL, INDEX IDX_9D829F1597CD2A32 (trading_id), INDEX IDX_9D829F1553C674EE (offer_id), PRIMARY KEY(trading_id, offer_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE trading_offer ADD CONSTRAINT FK_9D829F1553C674EE FOREIGN KEY (offer_id) REFERENCES offer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE trading_offer ADD CONSTRAINT FK_9D829F1597CD2A32 FOREIGN KEY (trading_id) REFERENCES trading (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE offer DROP FOREIGN KEY FK_29D6873E97CD2A32');
        $this->addSql('DROP INDEX IDX_29D6873E97CD2A32 ON offer');
        $this->addSql('ALTER TABLE offer DROP trading_id');
    }
}
