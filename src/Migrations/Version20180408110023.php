<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180408110023 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE trading ADD category_id INT DEFAULT NULL, DROP category');
        $this->addSql('ALTER TABLE trading ADD CONSTRAINT FK_BC19FB5812469DE2 FOREIGN KEY (category_id) REFERENCES interest (id)');
        $this->addSql('CREATE INDEX IDX_BC19FB5812469DE2 ON trading (category_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE trading DROP FOREIGN KEY FK_BC19FB5812469DE2');
        $this->addSql('DROP INDEX IDX_BC19FB5812469DE2 ON trading');
        $this->addSql('ALTER TABLE trading ADD category VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, DROP category_id');
    }
}
