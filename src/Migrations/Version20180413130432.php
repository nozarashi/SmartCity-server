<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180413130432 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE offer ADD category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE offer ADD CONSTRAINT FK_29D6873E12469DE2 FOREIGN KEY (category_id) REFERENCES interest (id)');
        $this->addSql('CREATE INDEX IDX_29D6873E12469DE2 ON offer (category_id)');
        $this->addSql('ALTER TABLE trading DROP FOREIGN KEY FK_BC19FB5812469DE2');
        $this->addSql('DROP INDEX IDX_BC19FB5812469DE2 ON trading');
        $this->addSql('ALTER TABLE trading DROP category_id');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE offer DROP FOREIGN KEY FK_29D6873E12469DE2');
        $this->addSql('DROP INDEX IDX_29D6873E12469DE2 ON offer');
        $this->addSql('ALTER TABLE offer DROP category_id');
        $this->addSql('ALTER TABLE trading ADD category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE trading ADD CONSTRAINT FK_BC19FB5812469DE2 FOREIGN KEY (category_id) REFERENCES interest (id)');
        $this->addSql('CREATE INDEX IDX_BC19FB5812469DE2 ON trading (category_id)');
    }
}
