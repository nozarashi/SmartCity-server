<?php
/**
 * Created by PhpStorm.
 * User: raoul
 * Date: 04/03/18
 * Time: 17:01
 */

namespace App\Utils;

/**
 * Trait FormErrors
 * Formater les erreurs d'un formulaire Symfony de manière à ce qu'elles soient mieux accessible par le client
 * @package App\Utils
 */
trait FormErrors {


    public function getErrors($form) {
        $errors = array();
        $errors['message'] = 'Validation error';
        $errors['errors'] = [];
        foreach ($form as $fieldName => $formField) {
            $currentError = $formField->getErrors();

            if ($currentError->current()) {
                $current = $currentError->current();
                $errors['errors'][$fieldName][] = $current->getMessage();
            }
        }

        return $errors;
    }
}