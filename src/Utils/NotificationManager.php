<?php
/**
 * Created by PhpStorm.
 * User: raoul
 * Date: 28/04/18
 * Time: 16:36
 */

namespace App\Utils;


class NotificationManager {
    const FCM_URL = 'https://fcm.googleapis.com/fcm/send';
    const API_KEY = 'AAAAbP4jcK0:APA91bFrXOWskOBmY-q-ad-W5UftuUpmyRM3B0EoTbsMVgkGZQL50Y2qf5bSzASdV8yqc7Li5HqAgYLcWl-fa8VF5Kvq6ZwJOwSbOA4C-rSvgD2fw5vAHKqVE1BsjwTxUG0jAOfiO8mA';

    private static $instance = null;

    private function __construct() {
    }

    public static function getInstance(): NotificationManager {
        if (self::$instance === null) {
            self::$instance = new NotificationManager();
        }
        return self::$instance;
    }

    /**
     * Envoie une notification à une liste d'utilisateur
     * @param string $title
     * @param string $message
     * @param array $tokenList
     */
    public function sendNotification(string $title, string $message, array $tokenList) {
        $token = 'chejJ-d0BoA:APA91bFz0DIHCrn6pTef1bpFrP5_gQEo_8ie4R9i5E8obUAWstJjCHzzT2-pD_ckID4UUbgAGHdFCINA8Cv-QkJaWehbcBWjcCUN7vu6neb8h5Os1_C9sI5u83vLc2ZILEIYiEqGO3XY';

        $notification = [
            'title' => $title,
            'body' => $message,
            'sound' => 'default'
        ];
        $extraNotificationData = [
            "message" => $notification
        ];

        $fcmNotification = [
            'registration_ids' => $tokenList, //multple token array
            //  'to' => $token, //single token
            'notification' => $notification,
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization:key=' . self::API_KEY,
            'Content-Type: application/json; UTF-8'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::FCM_URL);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
    }
}