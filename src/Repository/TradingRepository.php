<?php

namespace App\Repository;

use App\Entity\Trading;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Trading|null find($id, $lockMode = null, $lockVersion = null)
 * @method Trading|null findOneBy(array $criteria, array $orderBy = null)
 * @method Trading[]    findAll()
 * @method Trading[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TradingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Trading::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('t')
            ->where('t.something = :value')->setParameter('value', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findUserTradings(int $userId, int $page = 1) {
        $query = $this->createQueryBuilder('t')
            ->where('t.user = :val')
            ->setParameter('val', $userId)
            ->getQuery();
        return $this->createPaginator($query, $page);
    }

    private function createPaginator($query, int $page): Pagerfanta {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage(10);
        $paginator->setCurrentPage($page);
        return $paginator;
    }

    public function getByIds($ids) {
        return $this->createQueryBuilder('t')
            ->select('t')
            ->andWhere('t.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findTradingsByName($name) {
        return $this->createQueryBuilder('t')
            ->select('t')
            ->join('t.location', 'l')
            ->addSelect('l')
            ->where('t.name LIKE :name OR t.completeAddress LIKE :name')
            ->orWhere('l.city = :param')
            ->orWhere('l.country = :param')
            ->setParameter('name', '%'.$name.'%')
            ->setParameter('param', $name)
            ->orderBy('t.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findTradingsByInterest($interest, $location = null) {
        if($location == null) {
            return $this->createQueryBuilder('t')
                ->select('t')
                ->join('t.offers', 'o')
                ->addSelect('o')
                ->join('o.category', 'c')
                ->addSelect('c')
                ->where('c.category = :interest')
                ->setParameter('interest', $interest)
                ->orderBy('t.name', 'ASC')
                ->getQuery()
                ->getResult();
        }
        return $this->createQueryBuilder('t')
            ->select('t')
            ->join('t.location', 'l')
            ->addSelect('l')
            ->join('t.offers', 'o')
            ->addSelect('o')
            ->join('o.category', 'c')
            ->addSelect('c')
            ->where('c.category = :interest')
            ->andwhere('l.id = :location')
            ->setParameter('interest', $interest)
            ->setParameter('location', $location)
            ->orderBy('t.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findAllTradings($location) {
        return $this->createQueryBuilder('t')
            ->select('t')
            ->join('t.location', 'l')
            ->where('l = :location')
            ->setParameter('location', $location)
            ->orderBy('t.name', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
