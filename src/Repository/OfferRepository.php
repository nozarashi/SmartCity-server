<?php

namespace App\Repository;

use App\Entity\Offer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Offer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Offer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Offer[]    findAll()
 * @method Offer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfferRepository extends ServiceEntityRepository {
    public function __construct(RegistryInterface $registry) {
        parent::__construct($registry, Offer::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('o')
            ->where('o.something = :value')->setParameter('value', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findTradingOffers(int $tradingId, int $page = 1) {
        $query = $this->createQueryBuilder('o')
            ->where('o.trading = :val')
            ->setParameter('val', $tradingId)
            ->getQuery();
        return $this->createPaginator($query, $page);
    }

    private function createPaginator($query, int $page): Pagerfanta {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage(10);
        $paginator->setCurrentPage($page);
        return $paginator;
    }

    public function findOffersByName($name, $location = null) {
        if ($location == null) {
            return $this->createQueryBuilder('o')
                ->select('o')
                ->join('o.category', 'c')
                ->where('o.brand LIKE :name OR o.product LIKE :name OR c.category = :categ')
                ->setParameter('name', '%' . $name . '%')
                ->setParameter('categ', $name)
                ->orderBy('o.product', 'ASC')
                ->getQuery()
                ->getResult();
        }
        return $this->createQueryBuilder('o')
            ->select('o')
            ->join('o.category', 'c')
            ->join('o.trading', 't')
            ->join('t.location', 'l')
            ->where('o.brand LIKE :name OR o.product LIKE :name OR c.category = :categ AND l.id = :location')
            ->setParameter('name', '%' . $name . '%')
            ->setParameter('categ', $name)
            ->setParameter('location', $location)
            ->orderBy('o.product', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findOffersByUserInterests($interests, $location, $tradings) {
        return $this->createQueryBuilder('o')
            ->select('o')
            ->join('o.trading', 't')
            ->where('o.category IN (:interests) OR t.id IN (:tradings)')
            ->andWhere('t.location = :location')
            ->setParameter('interests', $interests)
            ->setParameter('location', $location)
            ->setParameter('tradings', $tradings)
            ->getQuery()
            ->getResult();
    }
}
